use std::collections::HashMap;
use std::error::Error;

use prometheus::Registry;
use prometheusutil::Builder;
use weatherutil::{to_bars, to_farenheit, to_mph};

use clap::Parser;

#[derive(Parser, Debug)]
struct Args {
    #[clap(short, long, value_parser)]
    region_code: String,

    #[clap(short, long, value_parser)]
    push_gateway: String,
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    let data = weatherutil::request_weather_data(args.region_code.as_str())?;

    let registry = Registry::new();

    let builder = Builder::new(registry)
        .with_gauge("weather_temp_fahrenheit", "Temperature (F) at COS Weather Station", data.properties.temperature.converted(to_farenheit))
        .with_gauge("weather_relative_humidity_percent","Relative humidity (%) at COS Weather Station", data.properties.humidity.value)
        .with_gauge("weather_barometric_pressure", "Barometric pressure (Bars) at COS Weather Station", data.properties.barometric_pressure.converted(to_bars))
        .with_gauge("weather_wind_speed", "Wind speed (MPH) at COS Weather Station", data.properties.wind_speed.converted(to_mph))
        .with_gauge("weather_wind_gust", "Wind gust (MPH) at COS Weather Station", data.properties.wind_gust.converted(to_mph))
        .with_gauge("weather_max_temp_24_hours", "Max Temperature (F) at COS Weather Station over last 24 hours", data.properties.daily_high.converted(to_farenheit))
        .with_gauge("weather_min_temp_24_hours", "Min Temperature (F) at COS Weather Station over last 24 hours", data.properties.daily_low.converted(to_farenheit))
        .with_gauge("weather_heat_index", "Heat Index at COS Weather Station", data.properties.heat_index.converted(to_farenheit))
        .with_int_gauge("weather_wind_direction", "Wind direction (degrees) at COS Weather Station", data.properties.wind_direction.value.map(|v| v as i16));


    println!("Pushing metrics to gateway {}...", args.push_gateway);
    prometheus::push_metrics("weather_collector", HashMap::new(), args.push_gateway.as_str(), builder.get_metrics(), None)?;
    println!("Metrics posted");

    Ok(())
}