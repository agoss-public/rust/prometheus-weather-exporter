use prometheus::{register_gauge, register_int_gauge, Registry};
use prometheus::proto::MetricFamily;

#[cfg(test)]
mod test {
    fn test() {}
}

/// Struct to wrap around the Registry and make functionality for adding metrics "convenient"
pub struct Builder {
    pub registry: Registry,
}

impl Builder {
    pub fn new(registry: Registry) -> Self {
        Builder {
            registry
        }
    }

    /// Update this builder to include a new gauge metric storing a float value
    pub fn with_gauge<S: ToString>(self, name: S, desc: S, value: Option<f32>) -> Self {
        let gauge = register_gauge!(name.to_string(), desc.to_string());
        if let Ok(g) = gauge {
            g.set(value.unwrap_or(-1.0).into());
            if let Err(err) = self.registry.register(Box::new(g)) {
                println!("WARNING: unable to register new metric: {err}")
            }
        }
        self
    }

    /// Update this builder to include a new gauge metric storing an int value
    pub fn with_int_gauge<S: ToString>(self, name: S, desc: S, value: Option<i16>) -> Self {
        let gauge = register_int_gauge!(name.to_string(), desc.to_string());
        if let Ok(g) = gauge {
            g.set(value.unwrap_or(-1).into());
            if let Err(err) = self.registry.register(Box::new(g)) {
                println!("WARNING: unable to register new metric: {err}")
            }
        }
        self
    }

    pub fn get_metrics(self) -> Vec<MetricFamily> {
        self.registry.gather()
    }
}
