use reqwest::blocking::Response;
use reqwest::header::USER_AGENT;
use serde::Deserialize;
use thiserror::Error;

const API: &str = "https://api.weather.gov/stations";
const LATEST: &str = "observations/latest";

#[cfg(test)]
mod test {
    use std::fs;

    use crate::{send_weather_api_request, WeatherData};

    #[test]
    fn test_load_data() {
        let test_data = fs::read_to_string("../test-data/api-test-data.json");
        assert!(test_data.is_ok());
        let test_data = test_data.unwrap();

        let weather_data = serde_json::from_str::<WeatherData>(test_data.as_str());
        println!("{:?}", weather_data);
        assert!(weather_data.is_ok());
        let weather_data = weather_data.unwrap();

        assert_eq!(weather_data.properties.temperature.value, Some(28.3));
        assert_eq!(weather_data.properties.wind_speed.value, None);
        assert_eq!(weather_data.properties.wind_gust.value, Some(57.6));
        assert_eq!(weather_data.properties.barometric_pressure.value, Some(101930_f32));
    }

    #[test]
    fn test_connect_api() {
        let resp = send_weather_api_request("MRBW1");
        println!("{:?}", resp);
        assert!(resp.is_ok());
        let resp = resp.unwrap();
        let weather_data = resp.json::<WeatherData>();
        assert!(weather_data.is_ok());
    }
}

#[derive(Debug, Error)]
pub enum WeatherError {
    #[error("cannot send API request")]
    RequestFailed(#[from] reqwest::Error),

    #[error("API data returned cannot be decoded")]
    InvalidApiData(#[from] serde_json::Error),
}

#[derive(Debug, Deserialize)]
pub struct WeatherData {
    pub properties: WeatherDetail,
}

#[derive(Debug, Deserialize)]
pub struct WeatherDetail {
    pub temperature: UnitDesc,

    #[serde(rename = "relativeHumidity")]
    pub humidity: UnitDesc,
    #[serde(rename = "barometricPressure")]
    pub barometric_pressure: UnitDesc,
    #[serde(rename = "windSpeed")]
    pub wind_speed: UnitDesc,
    #[serde(rename = "windGust")]
    pub wind_gust: UnitDesc,
    #[serde(rename = "windDirection")]
    pub wind_direction: UnitDesc,
    #[serde(rename = "maxTemperatureLast24Hours")]
    pub daily_high: UnitDesc,
    #[serde(rename = "minTemperatureLast24Hours")]
    pub daily_low: UnitDesc,
    #[serde(rename = "heatIndex")]
    pub heat_index: UnitDesc,
}

#[derive(Debug, Deserialize)]
pub struct UnitDesc {
    pub value: Option<f32>,

    #[serde(rename = "unitCode")]
    pub code: String,
}

impl UnitDesc {
    pub fn converted(&self, converter: fn(f32) -> f32) -> Option<f32> {
        self.value.map(converter)
    }
}

pub fn request_weather_data(region_code: &str) -> Result<WeatherData, WeatherError> {
    let data = send_weather_api_request(region_code)?;
    serde_json::from_slice::<WeatherData>(&data.bytes()?).map_err(|e| WeatherError::InvalidApiData(e))
}

pub fn to_farenheit(celsius: f32) -> f32 {
    if celsius < -273.15 {
        -1.0
    } else {
        celsius * 9.0 / 5.0 + 32.0
    }
}

pub fn to_mph(kph: f32) -> f32 {
    kph / 1.60934
}

pub fn to_bars(pascal: f32) -> f32 {
    pascal * 0.010
}

fn send_weather_api_request(region_code: &str) -> Result<Response, reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let url = format!("{API}/{region_code}/{LATEST}");
    println!("Sending request to {url}");
    client.get(url)
        .header(USER_AGENT, "Mozilla/5.0")
        .send()
}